import csv
import re
import yaml

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
import wino

import model as M
from wines import categories


with open('secrets', 'rb') as s:
    secret = yaml.load(s)

APIKEY = secret['winecom_apikey']


def db_connect():
    Base = declarative_base()
    engine = create_engine('sqlite:///sommelyay.db')
    Base.metadata.bind = engine
    DBSession = sessionmaker(bind=engine)
    session = DBSession()
    return session


def cache_request(api_call):
    def cached_results(query, format='json'):
        result = M.query_wine(session, query)
        if result == None:  # Not pythonic, but result can be an empty list.
            result = api_call(query, format)
            M.store_wine(session, query, result)
        return result
    return cached_results


def safe_csv(row_maker):
    """Take csv src/dest and convert to csv reader/writer."""
    def opener(src, dst):
        with open(src, 'rb') as fp_src:
            with open(dst, 'wb') as fp_dst:
                rdr = csv.reader(fp_src)
                wrt = csv.writer(fp_dst)
                row_maker(rdr, wrt)
    return opener


@safe_csv
def run(rdr, wrt):
    header = []
    extra_fields = ['Wine.com Name', 'Varietal', 'Highest Rating', 'URL']

    for row in rdr:

        if not header:
            header = row + extra_fields
            wrt.writerow(header)
            print header
            continue

        if 'EFFECTIVE' in row[0]:
            # Skip useless dates row
            continue

        category = row[3]
        wine_name = row[5]

        if category not in categories:
            continue

        matches = lookup_wine(wine_name)
        if matches:
            print 'FOUND MATCHES! :D'
            try:
                wrt.writerow(row + matches[0])
                print row + matches[0]
                if len(matches) > 1:
                    for match in matches[1:]:
                        print ['' for c in range(len(row))] + match
                        wrt.writerow(['' for c in range(len(row))] + match)
            except Exception as e:
                print e
                continue


def lookup_wine(wine_name):
    searchable_wine_name = convert_abbrevs(strip_unicode(wine_name))
    print "Checking ", searchable_wine_name
    wines = get_wine(searchable_wine_name)
    matches = []
    for wine in wines:
        try:
            """Write wine.com result if query matches result name, or both
            vineyard and varietal are found in the query."""

            if searchable_wine_name not in strip_unicode(wine['Name']):

                if strip_unicode(wine['Varietal']['Name']) not in searchable_wine_name or \
                        strip_unicode(wine['Vineyard']['Name']) not in searchable_wine_name:
                    continue

            match = [
                strip_unicode(wine['Name']),
                strip_unicode(wine['Varietal']['Name']),
                wine['Ratings']['HighestScore'],
                wine['Url']]
            matches.append(match)

        except Exception as e:
            print e
            continue

    return matches


@cache_request
def get_wine(string, format='json'):
    c = wino.Catalog(APIKEY, format)
    results = c.search(string)
    return results


def strip_unicode(string):
    try:
        return string.encode('ascii', 'ignore').lower()
    except Exception as e:
        pass
    try:
        return ''.join([s.decode('utf-8', 'ignore').encode('ascii') for s in string]).lower()
    except Exception as e:
        print e
        raise


def convert_abbrevs(string):
    abbreviations = {
        "cab": "cabernet",
        "svgn": "sauvignon",
        "bl": "blanc",
        "brl": "barrel",
        "brdx": "bordeaux",
        "chard": "chardonnay",
        "rsv": "reserve",
        "mrlt": "merlot",
        "pnt": "pinot",
        "vly": "valley",
        "vyd": "vineyard",
        "vyds": "vineyards",
        "cntrl": "central",
        "nr": "noir",
        "vyd": "vineyard",
        "cst": "coast",
        "znf": "zinfandel",
        "znfd": "zinfandel",
        "zfdl": "zinfandel",
        "znfdl": "zinfandel"}

    # do regex; cross fingers
    for abb, actual in abbreviations.items():
        string = re.sub(r'(\b){}(\b)'.format(abb), actual, string.rstrip())
    return string


session = db_connect()
