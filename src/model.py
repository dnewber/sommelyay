import json

from sqlalchemy import Column, Integer, String
from sqlalchemy.ext.declarative import declarative_base


Base = declarative_base()


class Wine(Base):
    __tablename__ = 'wine'
    _id = Column(Integer, primary_key=True)
    qs = Column(String, nullable=False)  # Query String from ice data
    result = Column(String, nullable=False)


def query_wine(session, query_string):
    wine = session.query(Wine).filter(Wine.qs == query_string).first()
    if wine:
        result = json.loads(wine.result)
        return result
    return


def store_wine(session, query_string, result):
    result_string = json.dumps(result)
    new_wine = Wine(qs=query_string, result=result_string)
    session.add(new_wine)
    session.commit()
