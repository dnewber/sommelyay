import main


def test_strip_unicode():
    test_str = u'win\xe9 nam\xe8'
    assert main.strip_unicode(test_str) == 'win nam'


def test_convert_abbrevs():
    test_str = 'cabby cab svgn bl brl'
    assert main.convert_abbrevs(test_str) == 'cabby cabernet sauvignon blanc barrel'
