import argparse
from datetime import datetime

from src.main import run


parser = argparse.ArgumentParser(description='SommelYAY!')
parser.add_argument(
    dest='inventory_csv_path',
    type=str,
    help='Path to your inventory CSV file. ')

args = parser.parse_args()
dst_filename = '{}_results.csv'.format(datetime.utcnow().strftime('%Y%m%d'))


if __name__ == "__main__":
    run(src=args.inventory_csv_path, dst=dst_filename)
