# SommelYAY! #

A script that takes the New Hampshire Liquor Commission's monthly inventory csv, and appends [wine.com](www.wine.com) ratings/info to each row.

Written to help a friend automate the tedious task of researching and ordering new wines for his restaurant. 

### Use Instructions ###

1. Clone this repo

2. Create a virtualenv for the app to run in - ex: `mkvirtualenv sommelyay`

3. Install the requirements - `pip install -r requirements.txt`

4. Run the `sommelyay.py` file, supplying it with the path to your csv wine list.

    - `python sommelyay.py /Downloads/interesting_wines.csv`

5. Buy some good wine.